import java.util.Scanner;

class Bank {
    private String accno;
    private String name;
    private String address;
    private String created_date;
    private long balance;
    private String acc_type;
    private String acc_status;


    Scanner KB = new Scanner(System.in);

    void deposit() {
        long amt;
        System.out.println("Enter the amount you want to Deposit: ");
        amt = KB.nextLong();
        balance = balance + amt;
    }

    void withdrawal() {
        long amt;
        System.out.println("Enter the amount you want to withdraw: ");
        amt = KB.nextLong();
        if (balance >= amt) {
            balance = balance - amt;
        } else {
            System.out.println("Transaction failed due to less balance!!");
        }
    }
    
    void showAccount() {
        System.out.println("\n Account Number: "+accno + "\n Account Type: " + acc_type + "\n Owner Name: " + name + "\n Owner Address: " + address + "\n Account Created Date: " + created_date + "\n Account Balance: " + balance + "\n Account Status: " +acc_status);
    }

    boolean search(String acn) {
        if (accno.equals(acn)) {
            showAccount();
            return (true);
        }
        return (false);
    }

	void openAccount() {
	    System.out.print("Enter Account No: ");
	    accno = KB.nextLine();
	    System.out.print("Enter Account Type[Savings/Current]: ");
	    acc_type = KB.nextLine();
	    System.out.print("Enter Name: ");
	    name = KB.nextLine();
	    System.out.print("Enter Address: ");
	    address = KB.nextLine();
	    System.out.print("Enter Date: ");
	    created_date = KB.nextLine();
	    System.out.print("Enter Balance: ");
	    balance = KB.nextLong();
	    System.out.print("Enter Status: ");
	    acc_status = KB.next();
		}
	}
public class Main {
    public static void main(String arg[]) {
        @SuppressWarnings("resource")
		Scanner KB = new Scanner(System.in);
        System.out.println("-------------------------------------------------------");
    	System.out.println("                   XYZ BANK");
        System.out.print("Enter the number of accounts you want to insert: ");
        int n = KB.nextInt();
        Bank C[] = new Bank[n];
        for (int i = 0; i < C.length; i++) {
            C[i] = new Bank();
            C[i].openAccount();
        }

        int ch;
        do {
            System.out.println("\n Main Menu\n 1. Show All Customer Details\n 2. Details By Account No.\n 3. Deposit\n 4. Withdrawal\n 5. Exit ");
                System.out.println("Enter your Choice: "); 
                ch = KB.nextInt();
                switch (ch) {
                    case 1:
                        for (int i = 0; i < C.length; i++) {
                            C[i].showAccount();
                        }
                        break;

                    case 2:
                        System.out.print("Enter the Account Number you want to Search: ");
                        String acn = KB.next();
                        boolean found = false;
                        for (int i = 0; i < C.length; i++) {
                            found = C[i].search(acn);
                            if (found) {
                                break;
                            }
                        }
                        if (!found) {
                            System.out.println("Account does not exist !!");
                        }
                        break;

                    case 3:
                        System.out.print("Enter Account No : ");
                        acn = KB.next();
                        found = false;
                        for (int i = 0; i < C.length; i++) {
                            found = C[i].search(acn);
                            if (found) {
                                C[i].deposit();
                                break;
                            }
                        }
                        if (!found) {
                            System.out.println("Account does not exist !!");
                        }
                        break;

                    case 4:
                        System.out.print("Enter Account No : ");
                        acn = KB.next();
                        found = false;
                        for (int i = 0; i < C.length; i++) {
                            found = C[i].search(acn);
                            if (found) {
                                C[i].withdrawal();
                                break;
                            }
                        }
                        if (!found) {
                            System.out.println("Account does not exist !!");
                        }
                        break;

                    case 5:
                        System.out.println("                      ThankYou !!");
                        System.out.println("-------------------------------------------------------");
                    	System.out.println("                      XYZ BANK");
                        break;
                }
            }
            while (ch != 5);
        }
    }
